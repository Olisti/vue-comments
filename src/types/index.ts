export interface Comment {
    id?: number;
    name: string;
    text: string;
    dateCreate: Date;
    dateEdit?: Date;
}

export type Sort = 'desc' | 'asc';
