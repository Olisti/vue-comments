import { Comment, Sort } from '@/types';
import { ActionContext } from 'vuex';

interface State {
    comments: Comment[];
    lastID: number;
}

export default {
    namespaced: true,

    state: {
        comments: [],
        lastID: 1
    } as State,

    getters: {
        DATA: (state: State) => state.comments,
        DATA_SORTED: (state: State) => (sort: Sort) =>
            state.comments.sort(
                (a: Comment, b: Comment) =>
                    (new Date(sort === 'desc' ? b.dateCreate : a.dateCreate) as any) -
                    (new Date(sort === 'desc' ? a.dateCreate : b.dateCreate) as any)
            ),
        TOTAL: (state: State) => state.comments.length
    },

    actions: {
        GET: async (context: ActionContext<State, null>) => {
            const fileData: Comment[] = require('@/constants/data.json');
            const storeData: Comment[] = fileData
                ? fileData.map((comm: Comment) => {
                      const id: number = context.state.lastID;
                      context.commit('INCREASE_ID');
                      return {
                          id,
                          ...comm
                      };
                  })
                : [];
            context.commit('SET', storeData);
        },
        CREATE: async (context: ActionContext<State, null>, comment: Comment) => {
            const id: number = context.state.lastID;
            context.commit('INCREASE_ID');
            context.commit('ADD', {
                id,
                ...comment
            });
        },
        EDIT: async (context: ActionContext<State, null>, comment: Comment) => {
            context.commit('UPDATE', comment);
        },
        DELETE: async (context: ActionContext<State, null>, id: number) => {
            context.commit('REMOVE', id);
        }
    },

    mutations: {
        SET: (state: State, comments: Comment[]) => {
            state.comments = comments;
        },
        ADD: (state: State, comment: Comment) => {
            state.comments.push(comment);
        },
        UPDATE: (state: State, comment: Comment) => {
            const index: number = state.comments.findIndex((comm: Comment) => comm.id === comment.id);
            state.comments[index].text = comment.text;
            state.comments[index].dateEdit = new Date();
        },
        REMOVE: (state: State, id: number) => {
            const index: number = state.comments.findIndex((comm: Comment) => comm.id === id);
            state.comments.splice(index, 1);
        },
        INCREASE_ID: (state: State) => {
            state.lastID++;
        }
    }
};
