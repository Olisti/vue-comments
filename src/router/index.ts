import Vue from 'vue';
import VueRouter from 'vue-router';
import Comments from '../views/Comments.vue';

Vue.use(VueRouter);

const routes = [
    {
        path: '/comments',
        name: 'comments',
        component: Comments
    },
    {
        path: '/comments/:id',
        name: 'comments-id',
        component: Comments
    },
    {
        path: '*',
        redirect: '/comments'
    }
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

export default router;
