# Тестовое задание для фронтенда на Vue JS

## Цель задания

Необходимо реализовать интерфейс просмотра, добавления, редактирования и удаления комментариев на Vue JS.

### Используемые дополнительные билиотеки

[vue-textarea-autosize](https://www.npmjs.com/package/vue-textarea-autosize)

[vue-scrollto](https://www.npmjs.com/package/vue-scrollto)

### Модель комментария

```
id - ID комментария;
name - Имя автора;
text - Комментарий;
dateCreate - Дата создания.
```

### Добавление комментария

При добавлении комментария реализована валидация инпутов с выводом ошибки.

### Редактирование и удаление комментария

При редактировании и удалении комментария реализовано подтверждение действия пользователя.

### Переход к комментарию

```
/comments/{id комментария}
```

Выбранный комментарий будет подсвечен. \
При клике на id комментария произойдет переход на него. \
Если комментарий с указанным id не существует, параметр будет сброшен.

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
